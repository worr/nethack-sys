extern crate bindgen;
extern crate clap;
#[macro_use]
extern crate slog;
extern crate sloggers;

use clap::{App, Arg};
use sloggers::Build;
use std::fs;
use std::io;
use std::path::Path;
use std::process;

fn lastmsg<S: AsRef<str>>(log: slog::Logger, msg: S) {
    crit!(log, "{}", msg.as_ref());
}

fn main() {
    let log = sloggers::terminal::TerminalLoggerBuilder::new()
        .format(sloggers::types::Format::Compact)
        .build()
        .unwrap();

    let args = App::new("NetHack Binding Generator")
        .author("William Orr <will@worrbase.com>")
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .takes_value(true)
                .value_name("FILENAME")
                .default_value("src/gen.rs")
                .help("file to write bindings to"),
        )
        .arg(
            Arg::with_name("headers")
                .short("i")
                .long("input")
                .takes_value(true)
                .value_name("FILENAME")
                .default_value("resources/headers.h")
                .help("headers to generate bindings from"),
        )
        .arg(Arg::with_name("force").short("f").long("force").help(
            "overwrite existing bindings",
        ))
        .get_matches();

    let mut build = bindgen::builder();

    // we provide defaults, so unwrap is safe
    let gen_file = Path::new(args.value_of("output").unwrap());
    let header = Path::new(args.value_of("headers").unwrap());

    if fs::metadata(header).is_err() {
        lastmsg(
            log,
            format!("header file {} not found", header.to_str().unwrap_or("")),
        );
        process::exit(1);
    }

    build = build.clang_arg("-I.").header(header.to_str().unwrap());

    if args.is_present("force") || !fs::metadata(gen_file).is_ok() {
        match gen(gen_file, build, &log) {
            Ok(()) => info!(log, "wrote bindings"),
            Err(e) => {
                lastmsg(log, format!("failed to write bindings: {}", e));
                process::exit(2);
            }
        }
        lastmsg(log, "done!");
        process::exit(0);
    }

    crit!(log, "did not write bindings; file exists");
}

fn gen<P: AsRef<Path>>(
    filename: P,
    build: bindgen::Builder,
    log: &slog::Logger,
) -> Result<(), io::Error> {
    let gen_out = Box::new(io::BufWriter::new(fs::File::create(filename.as_ref())?));

    info!(log, "generating bindings -- this can take a very long time");
    match build.generate() {
        Ok(bindings) => bindings.write(gen_out),
        Err(()) => Err(io::Error::new(io::ErrorKind::Other, "Could not generate bidings")),
    }
}
